package video;

import base.CommonAPI;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Video extends CommonAPI
{
    WebDriverWait wait = new WebDriverWait(driver,10);
    JavascriptExecutor js = (JavascriptExecutor)driver;



    @FindBy(xpath = "//*[@id=\"play-pause-container\"]/div[1]")
    WebElement btn;
    @FindBy(css = "#video-wrapper > div > div.vr-controls-section-center-full.animated.fadeOut > div > div.player-controls-container > div.stream-gear")
    WebElement playBtn;

    public void btnTest() throws InterruptedException
    {
        WebElement iframe = driver.findElement(By.id("player"));
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe));

        By add = By.xpath("//*[@id=\"player31841.controls\"]/div/div[1]/div/div[1]/div/table/tbody/tr/td/div");

        wait.until(ExpectedConditions.visibilityOfElementLocated(add));


        driver.findElement(By.xpath("//*[@id=\"player31841.controls\"]/div/div[1]/div/div[2]/div/div[1]/div[1]/div[2]/div[2]/table/tbody/tr/td/canvas")).click();




    }
}
